<?php

use Illuminate\Database\Seeder;
use App\Entity\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::create([
    		'name' => 'Admin',
    		'email' => 'admin@admin.q',
    		'password' => Hash::make('qwertyui'),
    		'is_admin' => true
    	]);
    	
        User::create([
            'name' => 'test1',
            'email' => 'test1@test1.q',
            'password' => Hash::make('qwertyui'),
        ]);        


        factory(User::class, 10)->create();
    }
}
