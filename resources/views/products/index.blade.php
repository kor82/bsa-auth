@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

	@if (session('success'))
	    <div class="alert alert-success">
	        {{ session('success') }}
	    </div>
	@endif    

			<div class="col col-md-12">
				<a class="btn btn-success" href="{{ route('products.create') }}" role="button">
					Add
				</a>				
			</div>

			<div class="col col-md-12">
		        <ul>
			        @foreach ($products as $product)
			        	<li>
			        		<a href="{{ route('products.show', $product->id) }}">
			        			{{ $product->name }}
			        		</a>
			        	</li>
			        @endforeach
		        </ul>
			</div>
    </div>
</div>

@endsection
