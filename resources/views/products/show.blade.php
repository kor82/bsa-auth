@extends('layouts.app')

@section('content')

@include('errors')

	<div class="col col-md-6">

		<h1>Show product</h1>

		<table class="table">
			<thead>
				<tr>
					<th scope="col">id</th>
					<th scope="col">Name</th>
					<th scope="col">Price</th>
					<th  colspan="2" scope="col">Actions</th>
				</tr>			
			</thead>
			<tbody>
				<tr>
					<td>{{ $product->id }}</td>
					<td>{{ $product->name }}</td>
					<td>{{ $product->price }}</td>
					<td>
						@can('update', $product)
							<a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}" role="button">
								Edit
							</a>
						@endcan
					</td>
					<td>
						@can('delete', $product)
							<form  action="{{ route('products.destroy', $product->id) }}" method="POST">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>						
						@endcan
					</td>
				</tr>
			</tbody>
		</table>
	</div>
@endsection



