@extends('layouts.app')

@section('content')
{{-- expr --}}

@include('errors')

	<div class="col-md-6">
		<h1>Edit product</h1>

		<form action="{{ route('products.update', $product->id) }}" method="POST">
			<div class="form-group">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<input type="text" class="form-control" name="name" value="{{ $product->name }}">
				<input type="text" class="form-control" name="price" value="{{ $product->price }}">
			</div>

			<button type="submit" class="btn btn-default">Save</button>
		</form>

	</div>
@endsection



