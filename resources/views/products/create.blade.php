@extends('layouts.app')

@section('content')

@include('errors')

	<div class="col col-md-6">
		<h1>Add new product</h1>

		<form action="{{ route('products.store') }}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<input type="text" class="form-control"name="name" value="{{old('name')}}" placeholder="product name" autofocus>
			</div>			
			<div class="form-group">
				<input type="text" class="form-control"name="price" value="{{old('price')}}" placeholder="product price" autofocus>
			</div>

			<button type="submit" class="btn btn-default">Save</button>
		</form>


	</div>
@endsection



